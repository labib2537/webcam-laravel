<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImageController extends Controller
{

    public function getData(){

        $images = Image::latest()->get();
        return response()->json([
            'images' => $images
        ]);
        // view('index', compact('images'));

    }

    public function uploadImage(Request $request)
    {
        $base64data = $request->input('image');
    
        $image = explode('base64,',$base64data);
        $image = end($image);
        $image = str_replace(' ', '+', $image);
        
        
        
        $img = new Image();
        $img->uuid = Str::uuid();
        $img->base64 = $base64data;
        $img->save();

        $file = "uploads/" . $img->uuid  . '.png';
    
        Storage::disk('public')->put($file,base64_decode($image));
        
        return response()->json([
            'message' => 'Image uploaded successfully'
        ]);
        

    }

    public function deleteImage(Request $request)
    {
        // Validate the request
        $request->validate([
            'id' => 'required|exists:images,id'
        ]);

        try {
            // Find the image by ID
            $image = Image::findOrFail($request->id);

            // Get the file name from the image base64 data
            $base64data = $image->base64;
            $imageData = explode('base64,', $base64data);
            $imageData = end($imageData);
            $imageName = "uploads/" . $image->uuid . '.png';

            // Delete the file from storage
            Storage::disk('public')->delete($imageName);
            
            // Delete the image
            $image->delete();

            // Return success response
            return response()->json(['message' => 'Image deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return error response
            return response()->json(['message' => 'Failed to delete image'], 500);
        }
    }
}

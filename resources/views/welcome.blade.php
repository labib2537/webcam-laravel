<!DOCTYPE html>
<html>
<head>
    <title>Webcam Capture</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-6" align="center">
                <label>Capture live photo</label>
                <div id="my_camera" class="pre_capture_frame"></div>
                <input type="hidden" name="captured_image_data" id="captured_image_data">
                <br>
                <button class="btn btn-info btn-round btn-file" onclick="takeSnapshot()">Take Snapshot</button> 
            </div>
            <div class="col-lg-6" align="center">
                <label>Result</label>
                <div id="results">
                    <img style="width: 350px;" class="after_capture_frame" src="{{ asset('image_placeholder.jpg') }}" />
                </div>
                <br>
                <button type="button" class="btn btn-success" onclick="saveSnap()">Save Picture</button>
            </div>
            <div class="col-lg-12">
                <h4>Images</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Date</th>
                            <th scope="col">Image</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.24/webcam.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        Webcam.set({
            width: 350,
            height: 287,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach('#my_camera');

        function takeSnapshot() {
            Webcam.snap(function(data_uri) {
                document.getElementById('results').innerHTML = '<img class="after_capture_frame" src="' + data_uri + '"/>';
                $("#captured_image_data").val(data_uri);
            });
        }

        function saveSnap() {
            var base64data = $("#captured_image_data").val();
            console.log(base64data);
            $.ajax({
                type: "POST",
                url: "{{ url('/capture') }}",
                data: {
                    image: base64data,
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    Swal.fire({
                    icon: 'success',
                    title: 'Uploaded Successfully',
                    text: response.message
            });
        fetchData();

                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
        
        // Fetch data initially when the page loads
        fetchData();

        // Function to fetch data
        function fetchData() {
            $.ajax({
                type: "GET",
                url: "{{ url('/get_data') }}",
                success: function(response) {
                    var images = response.images;
                    $("tbody").empty(); // Clear existing table body
                    images.forEach(function(image, index) {
                        // Parse created_at timestamp to JavaScript Date object
                        var createdAt = new Date(image.created_at);
                        // Format date as desired (day/month/year at hours:minutes AM/PM)
                        var formattedDate = createdAt.toLocaleString('en-GB', { timeZone: 'Asia/Dhaka', hour12: true });

                        var row = "<tr>" +
                            "<th scope='row'>" + (index + 1) + "</th>" +
                            "<td>" + formattedDate + "</td>" +
                            "<td><img src='" + image.base64 + "' alt='Image'></td>" +
                            "<td><button class='btn btn-danger btn-sm' onclick='deleteImage(" + image.id + ")'>Delete</button></td>" +
                            "</tr>";
                        $("tbody").append(row);
                    });
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }


        function deleteImage(imageId) {
            $.ajax({
                type: "POST",
                url: "{{ route('delete.image') }}",
                data: {
                    id: imageId,
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted Successfully',
                        text: response.message
                    });
                    fetchData(); // Refresh the table after deleting the image
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }

    </script>
</body>
</html>

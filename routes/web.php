<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/test-route', function () {
//     return response()->json(['message' => 'Test route reached']);
// });


// Route::post('/capture-image-upload', 'App\Http\Controllers\ImageController@uploadImage')->name('capture.imageupload');
Route::post('/capture', [ImageController::class, 'uploadImage'])->name('capture.imageupload');
Route::get('/get_data', [ImageController::class, 'getData']);
Route::post('/delete_image', [ImageController::class, 'deleteImage'])->name('delete.image');

